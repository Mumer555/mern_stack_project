function ContactForm() {
  return (
    <>
      <form>
        <div class="row mb-4">
          <div class="col">
            <div class="form-outline">
              <input
                type="text"
                id="name"
                class="form-control"
                placeholder="Your name"
              />
            </div>
          </div>
          <div class="col">
            <div class="form-outline">
              <input
                type="email"
                id="email"
                class="form-control"
                placeholder="Your email"
              />
            </div>
          </div>
          <div class="col">
            <div class="form-outline">
              <input
                type="number"
                id="phone"
                class="form-control"
                placeholder="Your phone#"
              />
            </div>
          </div>
        </div>

        <div class="form-outline mb-4">
          <textarea
            class="form-control"
            id="message"
            rows="4"
            placeholder="Message"
          ></textarea>
        </div>

        <button type="submit" class="btn btn-primary btn-block mb-4">
          Send Message
        </button>
      </form>
    </>
  );
}

export default ContactForm;
