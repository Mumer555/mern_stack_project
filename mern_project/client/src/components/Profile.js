import fiverr from "../images/fiverr.jpg"
function Profile() {
    return ( 
        <>
            <div className="row m-5 ">
                <div className="col-lg-4">
                    <img style={{width:200 , height:200}} src={fiverr} alt="profileImage" />
                </div>
                <div className="col-lg-6">
                    <h1>M Umer Amjad</h1>
                    <h4>Web Developer</h4>
                    <p className="mt-3">💚You Will Never Walk Alone💚</p>
                </div>
                <div className="col-lg-2">
                    <button className="btn btn-sm btn-secondary">Edit Profile</button>
                </div>
            </div>
        </>
     );
}

export default Profile;