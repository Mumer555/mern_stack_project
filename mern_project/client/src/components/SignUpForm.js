import { BsFillPersonFill , BsFillTelephoneFill } from "react-icons/bs";
import { MdEmail } from "react-icons/md";
import { RiLockPasswordFill } from "react-icons/ri";

function SignUpForm() {
  return (
    <>
      <form>
        <div className="form-group">
          <div className="d-flex">
            <label className="mt-1 mx-1" for="name">
              <BsFillPersonFill />
            </label>
            <input
              type="text"
              className="form-control border-0"
              id="name"
              placeholder="Your Name"
            />
          </div>
          <hr style={{ height: 2, weidth: "100%" }} />
        </div>
        <div className="form-group">
          <div className="d-flex">
            <label className="mt-1 mx-1" for="email">
              <MdEmail />
            </label>
            <input
              type="email"
              className="form-control border-0"
              id="email"
              placeholder="Your Email"
            />
          </div>
          <hr style={{ height: 2, weidth: "100%" }} />
        </div>
        <div className="form-group">
          <div className="d-flex">
            <label className="mt-1 mx-1" for="phone">
              <BsFillTelephoneFill />
            </label>
            <input
              type="number"
              className="form-control border-0"
              id="phone"
              placeholder="Your Phone#"
            />
          </div>
          <hr style={{ height: 2, weidth: "100%" }} />
        </div>
        <div className="form-group">
          <div className="d-flex">
            <label className="mt-1 mx-1" for="password">
              <RiLockPasswordFill />
            </label>
            <input
              type="password"
              className="form-control border-0"
              id="password"
              placeholder="Password"
            />
          </div>
          <hr style={{ height: 2, weidth: "100%" }} />
        </div>
        <div className="form-group">
          <div className="d-flex">
            <label className="mt-1 mx-1" for="cpassword">
              <RiLockPasswordFill />
            </label>
            <input
              type="password"
              className="form-control border-0"
              id="cpassword"
              placeholder="Confirm Password"
            />
          </div>
          <hr style={{ height: 2, weidth: "100%" }} />
        </div>
        <input type="submit" className="btn btn-lg btn-primary my-3" value="Register" />
      </form>
    </>
  );
}

export default SignUpForm;
