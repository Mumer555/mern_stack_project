import { NavLink } from "react-router-dom";

function ErrorPage() {
  return (
    <>
      <div style={{ height: "89vh" }} className="row">
        <div className="m-auto text-center">
          <span style={{ fontSize: 100 }}>
            <b>404</b>
          </span>
          <h3 style={{ fontSize: 40 }} className="text-primary">
            Page Error
          </h3>
          <p>page you are looking for does not exist</p>
          <NavLink to="/">
            <button className="btn btn-primary">GoTo Home</button>
          </NavLink>
        </div>
      </div>
    </>
  );
}

export default ErrorPage;
