import SwitchInfo from "./SwitchInfo";

function Bio() {
  return (
    <>
      <div className="row m-5">
        <div className="col-lg-4 d-flex flex-column">
          <a href="https://www.youtube.com">YouTube</a>
          <a href="https://www.google.com">facebook</a>
          <a href="https://linkedin.com/in/muhammad-umer-amjad-928331213">
            LinkedIn
          </a>
          <a href="https://gitlab.com/Mumer555">GitLab</a>
          <a href="https://www.google.com">Google</a>
        </div>
        <div className="col-lg-8">
          <SwitchInfo />
        </div>
      </div>
    </>
  );
}

export default Bio;
