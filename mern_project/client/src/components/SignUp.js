import registration from "../images/registration.png";
import { NavLink } from "react-router-dom";
import SignUpForm from "./SignUpForm";
import Options from "./Options";

function SignUp() {
  return (
    <>
      <div className="container-fluid">
        <div
          style={{ height: "91vh", "background-color": "#f2f2f2" }}
          className="row"
        >
          <div className="col-lg-7 m-auto bg-white rounded">
            <div className="row">
              <div className="col-lg-6">
                <h2 className="text-center my-2">SignUp</h2>
                <SignUpForm />
              </div>
              <div className="col-lg-6 d-flex flex-column">
                <div className="my-auto">
                  <img
                    className="d-flex justify-content-center"
                    src={registration}
                    alt="registration"
                  />
                  <NavLink
                    className="d-flex justify-content-center mt-3"
                    to="/login"
                  >
                    already have an account
                  </NavLink>
                  <Options />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default SignUp;
