import login from "../images/login.png";
import { NavLink } from "react-router-dom";
import { MdEmail } from "react-icons/md";
import { RiLockPasswordFill } from "react-icons/ri";

function Login() {
  return (
    <>
      <div className="container-fluid">
        <div
          style={{ height: "91.5vh", "background-color": "#f2f2f2" }}
          className="row"
        >
          <div className="col-lg-7 m-auto bg-white rounded">
            <div className="row">
              <div className="col-lg-6 d-flex flex-column">
                <div className="my-auto">
                  <img
                    style={{ width: 300, height: 200 }}
                    className="d-flex justify-content-center"
                    src={login}
                    alt="registration"
                  />
                  <NavLink
                    className="d-flex justify-content-center mt-3"
                    to="/signup"
                  >
                    create an account
                  </NavLink>
                </div>
              </div>
              <div className="col-lg-6">
                <h2 className="text-center mt-2 mb-5">Sign In</h2>
                <form>
                  <div className="form-group">
                    <div className="d-flex">
                      <label className="mt-1 mx-1" for="email">
                        <MdEmail />
                      </label>
                      <input
                        type="email"
                        className="form-control border-0"
                        id="email"
                        placeholder="Your Email"
                      />
                    </div>
                    <hr style={{ height: 2, weidth: "100%" }} />
                  </div>
                  <div className="form-group">
                    <div className="d-flex">
                      <label className="mt-1 mx-1" for="password">
                        <RiLockPasswordFill />
                      </label>
                      <input
                        type="password"
                        className="form-control border-0"
                        id="password"
                        placeholder="Password"
                      />
                    </div>
                    <hr style={{ height: 2, weidth: "100%" }} />
                  </div>
                  <input
                    type="submit"
                    className="btn btn-lg btn-primary my-3"
                    value="LogIn"
                  />
                </form>
                <div class="my-2 d-flex">
                  <p className="m-2">Or Login with</p>
                  <button
                    type="button"
                    className="btn btn-secondary btn-floating mx-1"
                  >
                    <i className="fab fa-facebook-f"></i>
                  </button>

                  <button
                    type="button"
                    className="btn btn-danger btn-floating mx-1"
                  >
                    <i className="fab fa-google"></i>
                  </button>

                  <button
                    type="button"
                    className="btn btn-primary btn-floating mx-1"
                  >
                    <i className="fab fa-twitter"></i>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Login;
