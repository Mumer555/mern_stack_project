function Home() {
  return (
    <>
      <div className="container-fluid">
        <div
          style={{
            height: "91.5vh",
            "background-image": "radial-gradient(circle, white, #9999ff)",
          }}
          className="row"
        >
          <div className="m-auto text-center">
            <h1>Welcome</h1>
            <p>💚You Will Never Walk Alone💚</p>
          </div>
        </div>
      </div>
    </>
  );
}

export default Home;
