import Bio from "./Bio";
import Profile from "./Profile";

function About() {
  return (
    <>
      <div className="container-fluid">
        <div
          style={{ height: "91.5vh", "background-color": "#f2f2f2" }}
          className="row"
        >
          <div className="col-lg-8 m-auto rounded bg-light">
            <Profile />
            <Bio />
          </div>
        </div>
      </div>
    </>
  );
}

export default About;
