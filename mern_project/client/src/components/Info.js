import { BsPhoneFill, BsFillEnvelopeFill } from "react-icons/bs";
import { MdLocationPin } from "react-icons/md";

function Info() {
  return (
    <>
      <div className="col-lg-3 bg-white rounded">
        <div className="row">
          <div className="d-flex justify-content-around">
            <div className="col-lg-2">
              <BsPhoneFill
                className="mt-4"
                style={{ width: 25, height: 25, color: "blue" }}
              />
            </div>
            <div className="col-lg-8">
              <h5 className="mt-2">Phone</h5>
              <p>+92 312 6935581</p>
            </div>
          </div>
        </div>
      </div>
      <div className="col-lg-3 bg-white rounded">
        <div className="d-flex justify-content-around">
          <div className="col-lg-1">
            <BsFillEnvelopeFill
              className="mt-4"
              style={{ width: 25, height: 25, color: "blue" }}
            />
          </div>
          <div className="col-lg-10">
            <h5 className="mt-2">Email</h5>
            <p>muhammadumeramjad5@gmail.com</p>
          </div>
        </div>
      </div>
      <div className="col-lg-3 bg-white rounded">
        <div className="d-flex justify-content-around">
          <div className="col-lg-2">
            <MdLocationPin
              className="mt-4"
              style={{ width: 25, height: 25, color: "blue" }}
            />
          </div>
          <div className="col-lg-8">
            <h5 className="mt-2">Address</h5>
            <p>H# D-1 GCT SWL </p>
          </div>
        </div>
      </div>
    </>
  );
}

export default Info;
