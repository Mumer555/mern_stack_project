import { Tabs, Tab } from "react-bootstrap";

function SwitchInfo() {
  return (
    <>
      <Tabs defaultActiveKey="profile">
        <Tab eventKey="profile" title="Profile">
          <div className="row">
            <div className="col-lg-6">
              <h6>name</h6>
              <br />
              <h6>email</h6>
              <br />
              <h6>position</h6>
            </div>
            <div className="col-lg-6 text-primary">
              <h6>M Umer Amjad</h6>
              <br />
              <h6>muhammadumeramjad5@gmail.com</h6>
              <br />
              <h6>web developer</h6>
            </div>
          </div>
        </Tab>
        <Tab eventKey="timeline" title="Timeline">
          Tab content 2
        </Tab>
      </Tabs>
    </>
  );
}

export default SwitchInfo;
