import ContactForm from "./ContactForm";
import Info from "./Info";

function Contact() {
  return (
    <>
      <div
        style={{ "background-color": "#f2f2f2" }}
        className="container-fluid"
      >
        <div style={{ height: "20.5vh" }} className="row">
          <div className="m-auto d-flex justify-content-around">
            <Info />
          </div>
        </div>
        <div style={{ height: "71vh" }} className="row">
          <div className="col-lg-6 m-auto bg-white rounded">
            <div className="row">
              <div className="col-lg-10 m-auto">
                <h2 className="my-4">Get in Touch</h2>
                <ContactForm />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Contact;
