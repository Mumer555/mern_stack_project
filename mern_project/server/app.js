const dotenv = require("dotenv");
const express = require("express");
const app = express();

dotenv.config({path:"./.env"});

const PORT = process.env.PORT;

//connecting to database
require("./db/connect");

// middleware to convert object in json
app.use(express.json());

// applying auth middleware  Here it is calling router to get all routes
app.use(require("./router/auth"));

// middleware
const middleware = (req, res, next) => {
    console.log("hello from middleware");
    next();
}



// routes
//app.get("/" , (req , res) => {
//    res.send("Ramdan Mubarik to all the Muslims");
//});


app.get("/about" , middleware, (req , res) => {
    res.send("Welcome to About page from server");
});

app.get("/contact" , (req , res) => {
    res.send("Welcome to Contact page from server");
});

app.get("/signIn" , (req , res) => {
    res.send("Welcome to Login page from server");
});

app.get("/signUp" , (req , res) => {
    res.send("Welcome to Registration page from server");
});


// listening at localhost:5000
app.listen(PORT , () => {
    console.log(`listening at port # ${PORT}`);
});