const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt");

const User = require("../model/userSchema");

router.get("/" , (req , res) => {
    res.send("welcome to home page from router.js");
})

router.post("/register" , (req , res) => {
    const {name, email, phone, password, cpassword} = req.body;
    if(!name || !email || !phone || !password || !cpassword)
    {
        return res.status(422).json({"message" : "please fill out all the fields"});
    }
    if(password !== cpassword)
    {
        return res.status(422).json({"message" : "password dont match"});
    }
    User.findOne({email}).then((userExists) => {
        if(userExists)
        {
            return res.status(400).json({"message" : "user already exists with this email"});
        }
        const user = new User({name, email, phone, password, cpassword});
        user.save();
        return res.status(201).json({"message" : "successfully registered"});
    }).catch((err) => {
        return res.status(500).json({"err" : err});
    });
});

router.post("/login" , async (req , res) => {
    const {email , password} = req.body;
    if(!email || !password)
    {
        return res.status(422).json({"message" : "please fill out the credentials"});
    }
    try{
        const user = await User.findOne({email});
        if(!user)
        {
            res.status(400).json({"message" : "credentials donot match"});
        }
        else{
            const isPasswordMatch = await bcrypt.compare(password , user.password);
            if(isPasswordMatch)
            {
                const token = await user.getAuthToken();
                console.log(token);
                res.cookie("jwtoken" , token , {
                    expires : new Date(Date.now() + 5000000),
                    httpOnly : true
                });
                res.status(200).json({"message" : "login successfull"});
            }
            else{
                res.status(400).json({"message" : "credentials donot match"});
            }
        }
    }catch(err){
        res.status(500).json({"error" : err});
    }
});

module.exports = router;